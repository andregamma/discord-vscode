# Discord Presence
> Atualize seu status no Discord com a recentemente adicionada Rich Presence.

<div align="center">
	<p>
		<a href="https://marketplace.visualstudio.com/items?itemName=icrawl.discord-vscode">
			<img src="https://vsmarketplacebadge.apphb.com/version/icrawl.discord-vscode.svg" alt="VS Code Marketplace">
		</a>
		<a href="https://discord.gg/4aFThGU">
			<img src="https://discordapp.com/api/guilds/304034982475595776/embed.png" alt="Discord server">
		</a>
	</p>
</div>

## Recursos

* Mostra o que você está editando no VSCode sem nenhuma besteira envolvida
* Suporte para mais de 70 dos idiomas mais populares
* Ative/Desative a Rich Presence para áreas de trabalho individuais (habilitado por padrão)
* Reconexão automática depois de perder a Internet ou uma reinicialização/falha no Discord (o padrão é 20 tentativas de reconexão)
* Suporte de String Personalizada
* Respeita o limite de 15 segundos do Discord quando se trata de atualizar seu status
* Stable or Insiders build detection
* Debug mode detection
* Reconecta facilmente manualmente ao Discord

## A Rich Presence não vai aparecer depois que meu PC for colocado no modo suspeso/depois que eu perder a internet!

Ele só tentará se reconectar 20 vezes.<br>
Depois de atingir esse limite, você terá que clicar manualmente no botão `Reconectar ao Discord` no canto inferior esquerdo da janela.

## Contribuindo

1. [Fork the repository](https://github.com/iCrawl/discord-vscode/fork)!
2. Clone your fork: `git clone https://github.com/your-username/discord-vscode.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -am 'Add some feature'`
5. Push to the branch: `git push origin my-new-feature`
6. Submit a pull request :D

## Autor

**Discord Presence** © [iCrawl](https://github.com/iCrawl).<br>
Criado e mantido por iCrawl.

> GitHub [@iCrawl](https://github.com/iCrawl)
